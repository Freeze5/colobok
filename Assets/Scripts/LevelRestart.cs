﻿using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class LevelRestart : MonoBehaviour
{
    public static LevelRestart Instance;
	[SerializeField]
	private GameObject _GOPanel;
	[SerializeField]
	private Text _bestText, _totalText;
	[SerializeField]
	private Player _player;
	private float _bestScore;
	private float _totalScore;
	private string _bestPreText = "BestScore: ";
	private string _totalPreText = "TotalScore: ";

	private string SAVE_BEST_SCORE = "BestSCORE";

	public event Action OnRestart;
	
    private void Awake()
    {
		if(Instance == null)
		{
			Instance = this;
		}
		else
		{
			Destroy(this.gameObject);
		}
      
		_GOPanel.SetActive(false);
    }

	private void OnEnable()
	{
		Player.OnPlayerDead += GameOver;
	}
	private void OnDisable()
	{
		Player.OnPlayerDead -= GameOver;
	}
	public void GameOver()
	{
		_GOPanel.SetActive(true);
		RevriteScore();
	}

	private void RevriteScore()
	{
		if (PlayerPrefs.GetFloat(SAVE_BEST_SCORE, 0) != 0)
		{
			_bestScore = PlayerPrefs.GetFloat(SAVE_BEST_SCORE);
			Debug.Log("Player doesn't play first time!");
		}
		else
		{
			_bestScore = 0;
			Debug.Log("Player playes First time!");
		}
		_totalScore = _player.GetScore;
		if(_totalScore > _bestScore)
		{
			_bestScore = _totalScore;
			PlayerPrefs.SetFloat(SAVE_BEST_SCORE, _bestScore);
		}

		_bestText.text = _bestPreText + _bestScore.ToString("0");
		_totalText.text = _totalPreText + _totalScore.ToString("0");
	}

    //called from Editor
    public void Restart ( int id)
    {
		if (OnRestart != null)
			OnRestart.Invoke();
		SceneManager.LoadScene(id);
	}
}
