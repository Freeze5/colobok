﻿using UnityEngine;

[CreateAssetMenu(menuName ="Friendly/Standart Friendly" , fileName ="New Friendly")]
public class FriendlyData : ScriptableObject {

    [SerializeField]
    private Sprite _mainSprite;
    public Sprite GetSprite { get { return _mainSprite; } }

    [SerializeField]
    private float _speed;
    public float GetSpeed { get { return _speed; } }

    [SerializeField]
    private float _gain;
    public float GetGain {  get { return _gain; } }
}
