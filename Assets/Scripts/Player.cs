﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {

    [SerializeField]
    private Vector3 _startPos;
	[SerializeField]
	private int _lives;
    [SerializeField]
    private Text _textScore , _textLives;
    private string _preTextScore = "Score: ";
	private string _preTextLives = "Lives: ";
	private float _score;
	public float GetScore { get { return _score; } }

	public static event Action OnPlayerDead;
	public static event Action OnPlayerGetHit;
	public static event Action OnPlayerCoin;

	[Range(0.01f,0.5f)]
    [SerializeField]
    private float _speed = 0.1f;

    [SerializeField]
    private Transform _endZone;

	private Color BasePlayerColor;

    private void Start()
    {
		BasePlayerColor = Color.white;
        transform.position = _startPos;
        _textScore.text = _preTextScore + _score.ToString();
		_textLives.text = _preTextLives + _lives.ToString();
    }

    private void FixedUpdate()
    {
        if(transform.position.y < _endZone.position.y)
        {
            LevelRestart.Instance.Restart(0);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var obj = collision.gameObject;

        if (Spawner.Instance.Enemies.ContainsKey(obj))
        {
			_lives--;
			_textLives.text = _preTextLives + _lives.ToString("0");
			if(OnPlayerGetHit != null)
				OnPlayerGetHit.Invoke();
			StartCoroutine( ChangePlayerColor(Color.red));
			if (_lives <= 0)
            {
                Destroy(gameObject);
				if(OnPlayerDead != null)
					OnPlayerDead.Invoke();

				Debug.Log("You died , gg!"); 
            }
            else
            {
                Debug.Log("Hero took damage! " + _lives + "hp left");
            }
        }
        if (Spawner.Instance.Friends.ContainsKey(obj))
        {
			if (OnPlayerCoin != null)
				OnPlayerCoin.Invoke();
			StartCoroutine( ChangePlayerColor(Color.green));
            _score += Spawner.Instance.Friends[obj].Gain;
            _textScore.text = _preTextScore + _score.ToString("0");
            
        }
    }

	private IEnumerator ChangePlayerColor( Color color)
	{
		this.gameObject.GetComponent<SpriteRenderer>().color = color;
		yield return new WaitForSeconds(0.2f);
		this.gameObject.GetComponent<SpriteRenderer>().color = BasePlayerColor;
	}

}
