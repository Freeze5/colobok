﻿using UnityEngine;
using System;

public class Enemy : MonoBehaviour {

    private EnemyData _enemyData;
    [SerializeField]
    private GameObject _endZone;

    public float Attack { get { return _enemyData.GetAttack; } }

    public static Action<GameObject> OnEnemyReturnToPool;

    public void Initialize(EnemyData _data)
    {
        _enemyData = _data;
        GetComponent<SpriteRenderer>().sprite = _enemyData.GetSprite;
		GetComponent<SpriteRenderer>().size = new Vector2(1, 1);
     
        _endZone = GameObject.FindGameObjectWithTag("endZone");
    }

    private void FixedUpdate()
    {
        transform.Translate(Vector3.down * _enemyData.GetSpeed);
        if(transform.position.y < _endZone.transform.position.y && OnEnemyReturnToPool != null)
        {
            OnEnemyReturnToPool(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            OnEnemyReturnToPool(this.gameObject);
        }
    }
}
