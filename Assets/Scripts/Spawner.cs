﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public static Spawner Instance;

    [Tooltip("List of settings for enemies")]
    [SerializeField]
    private EnemyDataBase _enemySettings;

    [Tooltip("List of settings for friendly")]
    [SerializeField]
    private List<FriendlyData> _friendlySettings;

    [Tooltip("Amount of the objects in the pool")]
    [SerializeField]
    private int _enemyPoolCount , _friendlyPoolCount;

    [Tooltip("Pool container")]
    [SerializeField]
    private Transform _enemyPool , _friendlyPool;

    [Tooltip("Reference to the parent enemy prefab")]
    [SerializeField]
    private GameObject _enemyPrefab , _friendlyPrefab;

    [Tooltip("Time btw enemy spawn")]
    [SerializeField]
    private float _spawnTime;

    /// <summary>
    /// Dictionaty for enemies scripts on the scene and we dont need to call GetComponent all the time
    /// </summary>
    public  Dictionary<GameObject, Enemy> Enemies ;

    public  Dictionary<GameObject, Friendly> Friends;

    private Queue<GameObject> _currentEnemy;

    private Queue<GameObject> _currentFriend;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        //enemies

        Enemies = new Dictionary<GameObject, Enemy>();
        _currentEnemy = new Queue<GameObject>();

        //friends
         Friends = new Dictionary<GameObject, Friendly>();     
        _currentFriend = new Queue<GameObject>();

        for(int i =0; i < _enemyPoolCount; i++)
        {
            var enemyPrefab = Instantiate(_enemyPrefab);
			enemyPrefab.gameObject.transform.SetParent(_enemyPool);	
			var script = enemyPrefab.GetComponent<Enemy>();
            enemyPrefab.SetActive(false);
            Enemies.Add(enemyPrefab, script);
            _currentEnemy.Enqueue(enemyPrefab);
        }

        for (int i = 0; i < _friendlyPoolCount; i++)
        {
            var friendPrefab = Instantiate(_friendlyPrefab);
            friendPrefab.gameObject.transform.SetParent(_friendlyPool);
            var script = friendPrefab.GetComponent<Friendly>();

            friendPrefab.SetActive(false);

            Friends.Add(friendPrefab, script);

            _currentFriend.Enqueue(friendPrefab);
        }

        StartCoroutine(Spawn());
        Enemy.OnEnemyReturnToPool += ReturnEnemy;
        Friendly.OnFriendlyReturnToPool += ReturnFriendly;
        
    }

    /// <summary>
    /// returns enemy into the pool
    /// </summary>
    /// <param name="_enemy"></param>
    void ReturnEnemy( GameObject _enemy)
    {
        _enemy.transform.position = this.transform.position;
        _enemy.SetActive(false);
        _currentEnemy.Enqueue(_enemy);
    }

    void ReturnFriendly(GameObject _friendly)
    {
        _friendly.transform.position = this.transform.position;
        _friendly.SetActive(false);
        _currentFriend.Enqueue(_friendly);
    }

    IEnumerator Spawn()
    {
        if(_spawnTime == 0)
        {
            Debug.LogError("Spawn time didnt set up . Used standart property = 1 sec");
            _spawnTime = 1f;
        }

        while (true)
        {
            yield return new WaitForSeconds(_spawnTime);
            if(_currentEnemy.Count > 0 && _currentFriend.Count > 0)
            {
                //getting components and enemy activation
                var enemy = _currentEnemy.Dequeue();
                var script = Enemies[enemy];
                enemy.SetActive(true);
                //getting components and friendly activation
                var friendly = _currentFriend.Dequeue();
                var friendScript = Friends[friendly];
                friendly.SetActive(true);
                //initialization of random FriendlyData 
                int random = Random.Range(0, _friendlySettings.Count);
                friendScript.Init(_friendlySettings[random]);
                //initialization of random EnemyData 
                script.Initialize(_enemySettings.GetRandomElement());

                //set up random spawn position  
                float xPos = Random.Range(-GameCamera.GetBorder +0.8f, GameCamera.GetBorder -0.8f);
                float xPos2 = Random.Range(-GameCamera.GetBorder + 0.8f, GameCamera.GetBorder - 0.8f);
                enemy.transform.position = new Vector2(xPos, transform.position.y);
                friendly.transform.position = new Vector2(xPos2, transform.position.y);
            }
        }
    }


    private void OnDestroy()
    {
        Enemy.OnEnemyReturnToPool -= ReturnEnemy;
        Friendly.OnFriendlyReturnToPool -= ReturnFriendly;
    }
}
