﻿using System;
using UnityEngine;

public class Friendly : MonoBehaviour
{
    private FriendlyData _friendlyData; 

    public float Gain { get { return _friendlyData.GetGain; } }

    public static Action<GameObject> OnFriendlyReturnToPool;

    [SerializeField]
    private GameObject _endZone; 

    public void Init(FriendlyData data)
    {
        _friendlyData = data;
        GetComponent<SpriteRenderer>().sprite = _friendlyData.GetSprite;
		GetComponent<SpriteRenderer>().size = new Vector2(1, 1);
		
		_endZone = GameObject.FindGameObjectWithTag("endZone");
    }

    private void FixedUpdate()
    {
        transform.Translate(Vector3.down * _friendlyData.GetSpeed);
        if (transform.position.y < _endZone.transform.position.y && OnFriendlyReturnToPool != null)
        {
            OnFriendlyReturnToPool(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            OnFriendlyReturnToPool(this.gameObject);
        }
    }

}
