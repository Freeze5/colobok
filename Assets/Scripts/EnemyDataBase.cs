﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Databases/Enemies" , fileName = "Enemies")]
public class EnemyDataBase : ScriptableObject {

    [SerializeField ]
    private List<EnemyData> _enemyList;
    [SerializeField]
    private EnemyData _currentEnemy;

    private int _currentIndex = 0;

    public void AddElement()
    {
        if(_enemyList == null)
        {
            _enemyList = new List<EnemyData>();
        }

        _currentEnemy = new EnemyData();
        _enemyList.Add(_currentEnemy);
        _currentIndex = _enemyList.Count - 1; // нужно отминусовывать на один т.к в листе перечисление элементов стартует с 0
    }

    public void RemoveCurrentElement()
    {
        //вместо создания нового обьекта подсовываем пердыдущий экземпляр (если он есть)
        if(_currentIndex > 0)
        {
            _currentEnemy = _enemyList[--_currentIndex];
            _enemyList.RemoveAt(++_currentIndex);
        }
        else
        {
            _enemyList.Clear();
            _currentEnemy = null;
        }
    }

    //простой индексатор
    public EnemyData this[int index]
    {
        get
        {
            if(_enemyList != null && index >= 0 && index < _enemyList.Count)
            {
                return _enemyList[index];
            }
            return null;
        }

        set
        {
            if (_enemyList == null)
                _enemyList = new List<EnemyData>();

            if (index >= 0 && index < _enemyList.Count && value != null)
                _enemyList[index] = value;
        }
    }

    public EnemyData GetNext()
    {
        if (_currentIndex < _enemyList.Count - 1)
            _currentIndex++;

        _currentEnemy = this[_currentIndex];
        return _currentEnemy;
    }

    public EnemyData GetPrev()
    {
        if (_currentIndex > 0)
            _currentIndex--;

        _currentEnemy = this[_currentIndex];
        return _currentEnemy;
    }

    public EnemyData GetRandomElement()
    {
        int random = Random.Range(0, _enemyList.Count);
        return _enemyList[random];
    }

    public void ClearDatabase()
    {
        _enemyList.Clear();
        _enemyList.Add(new EnemyData());
        _currentEnemy = _enemyList[0];
        _currentIndex = 0;
    }
}

[System.Serializable]
public class EnemyData
{

    [SerializeField]
    private Sprite _mainSprite;
    public Sprite GetSprite { get { return _mainSprite; } }

    [SerializeField]
    private float _speed;
    public float GetSpeed { get { return _speed; } }

    [SerializeField]
    private float _attack;
    public float GetAttack { get { return _attack; } }
}

