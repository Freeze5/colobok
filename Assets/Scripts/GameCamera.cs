﻿
using UnityEngine;

public class GameCamera : MonoBehaviour {

    private static float _border =0;

    /// <summary>
    /// Distance to the camera border
    /// </summary>
    public static float GetBorder
    {
        get
        {
            if (_border == 0)
            {
                var cam = Camera.main;
                _border = cam.aspect * cam.orthographicSize;
            }
            return _border;
        }
        private set { }
    }
}
