﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour
{
	public static SoundManager Instance;

	[SerializeField]
	private AudioClip _playerGetHit , _gameOverPopup , _restartPressed , _coinPickUp;
	[SerializeField]
	private AudioSource _audioSource;
	[SerializeField]
	private LevelRestart _lvlRestart;

	public float GetRestartLength { get { return _restartPressed.length; } }

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else
		{
			Destroy(this.gameObject);
		}
		//DontDestroyOnLoad(this.gameObject);
	}

	private void OnEnable()
	{
		Player.OnPlayerGetHit += PlayerGetHit;
		Player.OnPlayerDead += GameOverPopup;
		Player.OnPlayerCoin += CoinPickup;
		_lvlRestart.OnRestart += RestartPressed;
	}

	private void OnDisable()
	{
		Player.OnPlayerGetHit -= PlayerGetHit;
		Player.OnPlayerDead -= GameOverPopup;
		Player.OnPlayerCoin -= CoinPickup;
		_lvlRestart.OnRestart -= RestartPressed;
	}

	private void StopPlaying()
	{
		_audioSource.Stop();
	}

	private void StartPlying(AudioClip clip)
	{
		_audioSource.clip = clip;
		_audioSource.Play();
	}

	private void PlayerGetHit()
	{
		StartPlying(_playerGetHit);
	}

	private void GameOverPopup()
	{
		StartPlying(_gameOverPopup);
	}

	private void RestartPressed()
	{
		StartPlying(_restartPressed);
	}

	private void CoinPickup()
	{
		StartPlying(_coinPickUp);
	}
}
