﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragPlane : MonoBehaviour
{
    private float _speed = 150f;

    private void OnMouseDrag()
    {
        float rotateZ = Input.GetAxis("Mouse Y") * _speed * Mathf.Deg2Rad;
        transform.Rotate(Vector3.forward, rotateZ);
    }
}
