﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(EnemyDataBase))]
public class EnemyDatabaseEditor : Editor
{
    private EnemyDataBase _database;

    private void Awake()
    {
        _database = target as EnemyDataBase; // target - отображение скрипта , который мы переписываем
    }


    public override void OnInspectorGUI()
    {
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("RemoveAll!"))
        {
            _database.ClearDatabase();
        }
        if (GUILayout.Button("RemoveElement"))
        {
            _database.RemoveCurrentElement();
        }
        if (GUILayout.Button("AddElement"))
        {
            _database.AddElement();
        }
        if (GUILayout.Button("<=="))
        {
            _database.GetPrev();
        }
        if (GUILayout.Button("==>"))
        {
            _database.GetNext();
        }


        GUILayout.EndHorizontal();

        base.OnInspectorGUI();
    }
}
